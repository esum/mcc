open Cparse


type listpos = FIRST | MID | LAST | ONLY | AWAY | AFTER

let mon_op_repr = function
  | M_MINUS -> "-"
  | M_NOT -> "~"
  | M_POST_INC -> ".++"
  | M_POST_DEC -> ".--"
  | M_PRE_INC -> "++."
  | M_PRE_DEC -> "--."

let bin_op_repr = function
  | S_MUL -> "*"
  | S_DIV -> "/"
  | S_MOD -> "%"
  | S_ADD -> "+"
  | S_SUB -> "-"
  | S_INDEX -> ".[.]"

let cmp_op_repr = function
  | C_LT -> "<"
  | C_LE -> "<="
  | C_EQ -> "=="


(*let string_of_locator loc =
  let (name, fl, fc, ll, lc) = loc in
  name^"@"^(string_of_int fl)^","^(string_of_int fc)^":"^(string_of_int ll)^","^(string_of_int lc) *)
let string_of_locator loc = ""


let string_of_dec = function
  | CDECL(loc, var) -> Printf.sprintf "CDECL %s %s" var (string_of_locator loc)
  | _ -> failwith "Invalid_argument"

let print_declarations out dec_list =
  Format.fprintf out "Todo\n"


let print_locator out name fl fc ll lc = 
  Format.fprintf out "Todo\n"


let rec string_of_string_list = function
  | [] -> ""
  | h::t -> h^"\n"^(string_of_string_list t)

let find p l =
  try Some (List.find p l) with 
  | Not_found -> None

let rec print_utf8_string_list = function
  | h::t -> Utf8.print h; print_string "\n"; print_utf8_string_list t
  | _ -> ()

let rec print_string_option_list = function
  | (Some h)::t -> print_string h;  print_string_option_list t
  | None::t -> print_string "None\n"; print_string_option_list t
  | _ -> ()

let bd_h = "\xe2\x94\x80"    (* ─ *)
and bd_l = "\xe2\x95\xb4"    (* ╴ *)
and bd_dh = "\xe2\x94\xac"   (* ┬ *)
and bd_v = "\xe2\x94\x82"    (* │ *)
and bd_vr = "\xe2\x94\x9c"   (* ├ *)
and bd_ur = "\xe2\x94\x94"   (* └ *)
and bd_dl = "\xe2\x94\x90"   (* ┐ *)
and bd_ddhs = "\xe2\x95\xa5" (* ╥ *)
and bd_vd = "\xe2\x95\x91"   (* ║ *)
and bd_vdrs = "\xe2\x95\x9f" (* ╟ *)
and bd_udrs = "\xe2\x95\x99" (* ╙ *)
and bd_dbls = "\xe2\x95\x96" (* ╖ *)
and bd_adl = "\xe2\x95\xae"  (* ╮ *)
and bd_aur = "\xe2\x95\xb0"  (* ╰ *)


let string_list_of_ast_locate dec =

  let align_and_branch alignment_string l =
    let rec align_and_branch_aux first l = match first, l with
      | true, [h] -> [Printf.sprintf "%s%s" (alignment_string ONLY) h]
      | false, [h] -> [Printf.sprintf "%s%s" (alignment_string LAST) h]
      | true, h::t -> (Printf.sprintf "%s%s" (alignment_string FIRST) h)::(align_and_branch_aux false t)
      | false, h::t -> (Printf.sprintf "%s%s" (alignment_string MID) h)::(align_and_branch_aux false t)
      | _ -> [] in
  align_and_branch_aux true l in

  let align_and_branch_list alignment_string l =
    let rec align_and_branch_list_aux first l = match first, l with
      | true, [h::t] -> [(Printf.sprintf "%s%s" (alignment_string ONLY) h)::(List.map ((^) (alignment_string AFTER)) t)]
      | false, [h::t] -> [(Printf.sprintf "%s%s" (alignment_string LAST) h)::(List.map ((^) (alignment_string AFTER)) t)]
      | true, (h::t')::t -> ((Printf.sprintf "%s%s" (alignment_string FIRST) h)::(List.map ((^) (alignment_string AWAY)) t'))::(align_and_branch_list_aux false t)
      | false, (h::t')::t -> ((Printf.sprintf "%s%s" (alignment_string MID) h)::(List.map ((^) (alignment_string AWAY)) t'))::(align_and_branch_list_aux false t)
      | _, []::t -> align_and_branch_list_aux first t
      | _ -> [] in
  align_and_branch_list_aux true l in

  let rec string_list_of_expr locator_e = match locator_e with
    | loc, VAR(var) -> [Printf.sprintf "VAR %s %s" var (string_of_locator loc)]

    | loc, CST(n) -> [Printf.sprintf "CST %Ld %s" n (string_of_locator loc)]

    | loc, STRING(str) -> [Printf.sprintf "STRING \"%s\" %s" (String.escaped str) (string_of_locator loc)]

    | loc, SET_VAR(var, e) ->
      let e_strings = string_list_of_expr e in
      let alignment_len = String.length "SET_VAR " in
      let alignment_string_expr = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_aur^bd_h^" " (* "╰─ " *)
        | _ -> (String.make alignment_len ' ')^"   " (* "   " *) in
      (Printf.sprintf "SET_VAR %s%s %s" bd_dh bd_adl (string_of_locator loc)):: (* "SET_VAR ┬╮" *)
      (Printf.sprintf "        %s%s%s %s" bd_v bd_aur bd_h var):: (* "│╰─ " *)
      (align_and_branch alignment_string_expr e_strings)

    | loc, SET_ARRAY(var, e1, e2) ->
      let e1_strings = string_list_of_expr e1 in
      let e2_strings = string_list_of_expr e2 in
      let alignment_len = String.length "SET_ARRAY " in
      let alignment_string_expr1 = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_v^bd_aur^bd_h^" " (* "│╰─ " *)
        | _ -> (String.make alignment_len ' ')^bd_v^"   " (* "│   " *) in
      let alignment_string_expr2 = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_aur^bd_h^" " (* "╰─ " *)
        | _ -> (String.make alignment_len ' ')^"   " (* "   " *) in
      (Printf.sprintf "SET_ARRAY %s%s%s %s" bd_dh bd_dh bd_adl (string_of_locator loc)):: (* "SET_ARRAY ┬┬╮" *)
      (Printf.sprintf "          %s%s%s%s %s" bd_v bd_v bd_aur bd_h var)::( (* "││╰─ " *)
      (align_and_branch alignment_string_expr1 e1_strings)@
      (align_and_branch alignment_string_expr2 e2_strings))

    | loc, CALL(func, []) -> [Printf.sprintf "CALL %s %s" func (string_of_locator loc)]

    | loc, CALL(func, exprs) ->
      let exprs_strings_lists = List.map string_list_of_expr exprs in
      let alignment_len = String.length "CALL " in
      let alignment_string_exprs = function
        | AFTER -> (String.make alignment_len ' ')^"   " (* "   " *)
        | AWAY -> (String.make alignment_len ' ')^bd_v^"  " (* "│  " *)
        | LAST | ONLY -> (String.make alignment_len ' ')^bd_aur^bd_h^" " (* "╰─ " *)
        | _ -> (String.make alignment_len ' ')^bd_vr^bd_h^" " (* "├─ " *) in
      (Printf.sprintf "CALL %s%s %s" bd_dh bd_adl (string_of_locator loc)):: (* "CALL ┬╮" *)
      (Printf.sprintf "     %s%s%s %s" bd_v bd_aur bd_h func)::( (* "│╰─ " *)
      (List.concat (align_and_branch_list alignment_string_exprs exprs_strings_lists)))

    | loc, OP1(op, e) ->
      let op_string = mon_op_repr op in
      let e_strings = string_list_of_expr e in
      let alignment_len = String.length (op_string^" ") in
      let alignment_string_expr = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_aur^bd_h^" " (* "╰─ " *)
        | _ -> (String.make alignment_len ' ')^"   " (* "   " *) in
      (Printf.sprintf "%s %s %s" op_string bd_adl (string_of_locator loc)):: (* "OP1 ╮" *)
      (align_and_branch alignment_string_expr e_strings)

    | loc, OP2(op, e1, e2) ->
      let op_string = bin_op_repr op in
      let e1_strings = string_list_of_expr e1 in
      let e2_strings = string_list_of_expr e2 in
      let alignment_len = String.length (op_string^" ") in
      let alignment_string_expr1 = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_v^bd_aur^bd_h^" " (* "│╰─ " *)
        | _ -> (String.make alignment_len ' ')^bd_v^"   " (* "│   " *) in
      let alignment_string_expr2 = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_aur^bd_h^" " (* "╰─ " *)
        | _ -> (String.make alignment_len ' ')^"   " (* "   " *) in
      (Printf.sprintf "%s %s%s %s" op_string bd_dh bd_adl (string_of_locator loc))::( (* "OP2 ┬╮" *)
      (align_and_branch alignment_string_expr1 e1_strings)@
      (align_and_branch alignment_string_expr2 e2_strings))

    | loc, CMP(op, e1, e2) ->
      let op_string = cmp_op_repr op in
      let e1_strings = string_list_of_expr e1 in
      let e2_strings = string_list_of_expr e2 in
      let alignment_len = String.length (op_string^" ") in
      let alignment_string_expr1 = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_v^bd_aur^bd_h^" " (* "│╰─ " *)
        | _ -> (String.make alignment_len ' ')^bd_v^"   " (* "│   " *) in
      let alignment_string_expr2 = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_aur^bd_h^" " (* "╰─ " *)
        | _ -> (String.make alignment_len ' ')^"   " (* "   " *) in
      (Printf.sprintf "%s %s%s %s" op_string bd_dh bd_adl (string_of_locator loc))::( (* "CMP ┬╮" *)
      (align_and_branch alignment_string_expr1 e1_strings)@
      (align_and_branch alignment_string_expr2 e2_strings))

    | loc, EIF(e1, e2, e3) ->
      let e1_strings = string_list_of_expr e1 in
      let e2_strings = string_list_of_expr e2 in
      let e3_strings = string_list_of_expr e3 in
      let alignment_len = String.length "EIF " in
      let alignment_string_expr1 = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_v^bd_v^bd_aur^bd_h^" " (* "││╰─ " *)
        | _ -> (String.make alignment_len ' ')^bd_v^bd_v^"   " (* "││   " *) in
      let alignment_string_expr2 = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_v^bd_aur^bd_h^" " (* "│╰─ " *)
        | _ -> (String.make alignment_len ' ')^bd_v^"   " (* "│   " *) in
      let alignment_string_expr3 = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_aur^bd_h^" " (* "╰─ " *)
        | _ -> (String.make alignment_len ' ')^"   " (* "   " *) in
      (Printf.sprintf "EIF %s%s%s %s" bd_dh bd_dh bd_adl (string_of_locator loc))::( (* "EIF ┬┬╮" *)
      (align_and_branch alignment_string_expr1 e1_strings)@
      (align_and_branch alignment_string_expr2 e2_strings)@
      (align_and_branch alignment_string_expr3 e3_strings))

    | loc, ESEQ([]) -> [Printf.sprintf "skip %s" (string_of_locator loc)]

    | loc, ESEQ(exprs) ->
      let exprs_strings_lists = List.map string_list_of_expr exprs in
      let alignment_len = String.length "ESEQ " in
      let alignment_string_exprs = function
        | AFTER -> (String.make alignment_len ' ')^"   " (* "   " *)
        | AWAY -> (String.make alignment_len ' ')^bd_v^"  " (* "│  " *)
        | LAST -> (String.make alignment_len ' ')^bd_aur^bd_h^" " (* "╰─ " *)
        | _ -> (String.make alignment_len ' ')^bd_vr^bd_h^" " (* "├─ " *) in
      (Printf.sprintf "ESEQ %s %s" bd_adl (string_of_locator loc)):: (* "ESEQ ╮" *)
      (List.concat (align_and_branch_list alignment_string_exprs exprs_strings_lists)) in
  
  let rec string_list_of_code locator_code = match locator_code with
    | loc, CBLOCK(dec_list, c) ->
      let dec_strings = List.map string_of_dec dec_list in
      let c_strings = List.map string_list_of_code c in
      let alignment_len = String.length "CBLOCK " in
      let h = match dec_list, c with
        | [], [] -> Printf.sprintf "CBLOCK %s" (string_of_locator loc) (* "CBLOCK" *)
        | _, [] -> Printf.sprintf "CBLOCK %s %s" bd_dl (string_of_locator loc) (* "CBLOCK ┐" *)
        | [], _ -> Printf.sprintf "CBLOCK %s %s" bd_dbls (string_of_locator loc) (* "CBLOCK ╖" *)
        | _ -> Printf.sprintf "CBLOCK %s%s%s %s" bd_ddhs bd_h bd_dl (string_of_locator loc) (* "CBLOCK ╥─┐" *) in
      let aligned_dec_strings = match c with
        | [] -> 
          let alignment_string_dec = function
            | LAST | ONLY -> (String.make alignment_len ' ')^bd_ur^bd_h^" " (* "└─ " *)
            | _ -> (String.make alignment_len ' ')^bd_vr^bd_h^" " (* "├─ " *) in
          align_and_branch alignment_string_dec dec_strings
        | _ ->
          let alignment_string_dec = function
            | LAST | ONLY -> (String.make alignment_len ' ')^bd_vd^" "^bd_ur^bd_h^" " (* "║ └─ " *)
            | _ -> (String.make alignment_len ' ')^bd_vd^" "^bd_vr^bd_h^" " (* "║ ├─ " *) in
          align_and_branch alignment_string_dec dec_strings in
      let alignment_string_code = function
        | AFTER -> (String.make alignment_len ' ')^"   " (* "   " *)
        | AWAY -> (String.make alignment_len ' ')^bd_vd^"  " (* "║  " *)
        | LAST | ONLY -> (String.make alignment_len ' ')^bd_udrs^bd_h^" " (* "╙─ " *)
        | _ -> (String.make alignment_len ' ')^bd_vdrs^bd_h^" " (* "╟─ " *) in
      h::(
      aligned_dec_strings@
      (List.concat(align_and_branch_list alignment_string_code c_strings)))

    | loc, CEXPR(e) -> 
      let e_strings = string_list_of_expr e in
      let alignment_len = String.length "CEXPR " in
      let alignment_string_expr = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_aur^bd_h^" " (* "╰─ " *)
        | _ -> (String.make alignment_len ' ')^"   " (* "   " *) in
      (Printf.sprintf "CEXPR %s %s" bd_adl (string_of_locator loc))::(align_and_branch alignment_string_expr e_strings) (* "CEXPR ╮" *)

    | loc, CIF(e, c1, c2) ->
      let e_strings = string_list_of_expr e in
      let c1_strings = string_list_of_code c1 in
      let c2_strings = string_list_of_code c2 in
      let alignment_len = String.length "CIF " in
      let alignment_string_expr = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_vd^" "^bd_vd^" "^bd_aur^bd_h^" " (* "║ ║ ╰─ " *)
        | _ -> (String.make alignment_len ' ')^bd_vd^" "^bd_vd^"    " (* "║ ║    " *) in
      let alignment_string_code1 = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_vd^" "^bd_udrs^bd_h^" " (* "║ ╙─ " *)
        | _ -> (String.make alignment_len ' ')^bd_vd^"    " (* "║    " *) in
      let alignment_string_code2 = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_udrs^bd_h^" " (* "╙─ " *)
        | _ -> (String.make alignment_len ' ')^"   " (* "   " *) in
      (Printf.sprintf "CIF %s%s%s%s%s %s" bd_ddhs bd_h bd_ddhs bd_h bd_adl (string_of_locator loc))::( (* "CIF ╥─╥─╮" *)
      (align_and_branch alignment_string_expr e_strings)@
      (align_and_branch alignment_string_code1 c1_strings)@
      (align_and_branch alignment_string_code2 c2_strings))

    | loc, CWHILE(e, c) -> 
      let e_strings = string_list_of_expr e in
      let c_strings = string_list_of_code c in
      let alignment_len = String.length "CWHILE " in
      let alignment_string_expr = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_vd^" "^bd_aur^bd_h^" " (* "║ ╰─ " *)
        | _ -> (String.make alignment_len ' ')^bd_vd^"    " (* "║    " *) in
      let alignment_string_code = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_udrs^bd_h^" " (* "╙─ " *)
        | _ -> (String.make alignment_len ' ')^"   " (* "   " *) in
      (Printf.sprintf "CWHILE %s%s%s %s" bd_ddhs bd_h bd_adl (string_of_locator loc))::( (* "CWHILE ╥─╮" *)
      (align_and_branch alignment_string_expr e_strings)@
      (align_and_branch alignment_string_code c_strings))

    | loc, CRETURN(Some e) -> 
      let e_strings = string_list_of_expr e in
      let alignment_len = String.length "CRETURN " in
      let alignment_string_expr = function
        | FIRST | ONLY -> (String.make alignment_len ' ')^bd_aur^bd_h^" " (* "╰─ " *)
        | _ -> (String.make alignment_len ' ')^"   " (* "   " *) in
      (Printf.sprintf "CRETURN %s %s" bd_dl (string_of_locator loc))::(align_and_branch alignment_string_expr e_strings)

    | loc, CRETURN(None) -> [Printf.sprintf "CRETURN %s" (string_of_locator loc)] in

  let rec string_list_of_ast_aux = function
    | CDECL(loc, var) -> [string_of_dec (CDECL(loc, var))]
    | CFUN(loc, func, d_list, l_code) ->
      let branch_char = match d_list with
        | [] -> bd_l
        | _ -> bd_dl in
      let alignment_len = String.length ("CFUN "^func^" ") in
      let args_strings = List.map List.hd (List.map string_list_of_ast_aux d_list) in
      let code_strings = string_list_of_code l_code in
      let alignment_string_dec = function
        | LAST -> (String.make alignment_len ' ')^bd_vd^" "^bd_ur^bd_h^" " (* "║ └─ " *)
        | _ -> (String.make alignment_len ' ')^bd_vd^" "^bd_vr^bd_h^" " (* "║ ├─ " *) in
      let alignment_string_code = function
        | FIRST -> (String.make alignment_len ' ')^bd_udrs^bd_h^" " (* "╙─ " *)
        | _ -> (String.make alignment_len ' ')^"   " (* "   " *) in
      (Printf.sprintf "CFUN %s %s%s%s %s" func bd_ddhs bd_h branch_char (string_of_locator loc))::( (* "CFUN func ╥─┐" *)
      (align_and_branch alignment_string_dec args_strings)@
      (align_and_branch alignment_string_code code_strings)) in
  
  string_list_of_ast_aux dec


let compact l =
  let compact_chars u d = match u, d with
    | u, d when u = bd_dl && d = bd_vr -> Some bd_dh
    | u, d when u = bd_dl && d = bd_ur -> Some bd_h
    | u, d when u = bd_dh && d = bd_v -> Some bd_dh
    | u, d when u = bd_ddhs && d = bd_vd -> Some bd_ddhs
    | u, d when u = bd_dbls && d = bd_udrs -> Some bd_h
    | u, d when u = bd_dbls && d = bd_vd -> Some bd_dbls
    | u, d when u = bd_adl && d = bd_vr -> Some bd_dh
    | u, d when u = bd_adl && d = bd_aur -> Some bd_h
    | _ -> None in
  let compactable_symbols = [bd_dh; bd_dl; bd_ddhs; bd_dbls; bd_adl] in
  let compactable_positions s =
    let len = Utf8.length s in
    let rec compactable_positions_aux res = function
      | i when i = len -> res
      | i -> match find ((=) (Utf8.get s i)) compactable_symbols with
        | None -> compactable_positions_aux res (i+1)
        | Some utf8char -> compactable_positions_aux ((utf8char, i)::res) (i+1) in
    List.rev (compactable_positions_aux [] 0) in 
  let compact_step str1 str2 compactable_pos =
    let len = Utf8.length str1 in
    let rec compact_step_aux compactable_pos = function
      | i when i = len -> ()
      | i -> match compactable_pos with
        | [] ->
          Utf8.set str2 i (Utf8.get str1 i);
          compact_step_aux compactable_pos (i+1)
        | (_, j)::t when j<>i ->
          Utf8.set str2 i (Utf8.get str1 i);
          compact_step_aux compactable_pos (i+1)
        | (c, j)::t when j=i -> ( 
          match compact_chars c (Utf8.get str2 i) with
            | None -> failwith ""
            | Some new_char -> Utf8.set str2 i new_char; compact_step_aux t (i+1) )
        | _ -> failwith "" in
    compact_step_aux compactable_pos 0 in
  let rec compact_aux = function
    | h1::h2::t -> (
      let compactable_pos = compactable_positions h1 in
      match compactable_pos with
      | [] -> h1::(compact_aux (h2::t))
      | _ ->
        let test_compactability str1 str2 = function
          | _, i -> compact_chars (Utf8.get str1 i) (Utf8.get str2 i) in
        let compacted_chars = List.map (test_compactability h1 h2) compactable_pos in
        match List.mem None compacted_chars with
          | true -> h1::(compact_aux (h2::t))
          | false -> (compact_step h1 h2 compactable_pos); compact_aux (h2::t) )
    | l -> l in
  compact_aux l

let rec print_ast out dec_list =
  match dec_list with
  | h::t -> print_utf8_string_list (compact (List.map Utf8.utf8 (string_list_of_ast_locate h))); print_string "\n"; print_ast out t
  | [] -> ()
  (* Format.fprintf out "Todo\n" *)
