open Cparse
open Genlab

let (+) = Int64.add
and (-) = Int64.sub
and ( * ) = Int64.mul

let arg = List.nth ["%rdi"; "%rsi"; "%rdx"; "%rcx"; "%r8"; "%r9"]

type address = Offset of int64 | Symbol of string

type environment = (string * address) list

let current_function = ref ""

(* [next_address env] returns the next available address on the stack for the environment [env] *)
let next_address env =
  let rec next_address_aux off = function
    | (_, Offset n)::t when compare n off = -1 -> next_address_aux n t
    | _::t -> next_address_aux off t
    | _ -> Offset (off - 8L) in
  next_address_aux 0L env

let translate_address out = function
  | Offset n -> Printf.fprintf out "%Ld(%%rbp)" n
  | Symbol s -> Printf.fprintf out "%s" s

let assoc_env var env =
  try List.assoc var env with
  | _ -> Symbol var

(* [translate_expr out env e] translates the loc_expr [e] to assembly code in the environment [env] *)
let rec translate_expr out env = let translate_expr = translate_expr out in function
  | loc, VAR var ->
    Printf.fprintf out "\tmovq\t"; translate_address out (assoc_env var env); Printf.fprintf out ", %%rax # %s\n" var
  | loc, CST n ->
    Printf.fprintf out "\tmovq\t$%Ld, %%rax\n" n
  | loc, STRING s -> (
    let label = labelize loc in
    let predicate t = label = (fst t) in (
    match List.exists predicate env with
      | true -> ()
      | false ->
        Printf.fprintf out ".section .rodata\n";
        Printf.fprintf out "%s:\n" label;
        Printf.fprintf out "\t.string \"%s\"\n" (String.escaped s);
        Printf.fprintf out "\t.align 8\n";
        Printf.fprintf out ".text\n";
        Printf.fprintf out "\tmovq\t$%s, %%rax\n" label ) )
  | loc, SET_VAR (var, e) -> (
    translate_expr env e;
    Printf.fprintf out "\tmovq\t%%rax, "; translate_address out (assoc_env var env); Printf.fprintf out "\n" )
  | loc, SET_ARRAY(var, e1, e2) -> (
    translate_expr env e2;
    Printf.fprintf out "\tpushq\t%%rax\n";
    translate_expr env e1;
    Printf.fprintf out "\tmovq\t%%rax, %%rdi\n\tpopq\t%%rax\n";
    Printf.fprintf out "\tmovq\t"; translate_address out (assoc_env var env); Printf.fprintf out ", %%rdx\n";
    Printf.fprintf out "\tmovq\t%%rax, (%%rdx, %%rdi, 8)\n" )
  | loc, CALL(func, args) -> (
    push_args out env args;
    Printf.fprintf out "\txorq\t%%rax, %%rax\n";
    Printf.fprintf out "\tcall\t%s\n" func )
  | loc, OP1(op, e) -> (
    translate_expr env e;
    match op with
    | M_MINUS -> Printf.fprintf out "\tnegq\t%%rax\n"
    | M_NOT -> Printf.fprintf out "\tnotq\t%%rax\n"
    | M_POST_INC -> (
      match e with
      | loc, VAR var -> (
        Printf.fprintf out "\tincq\t"; translate_address out (assoc_env var env); Printf.fprintf out "\n" )
      | loc, OP2 (S_INDEX, e1, e2) -> (
        Printf.fprintf out "\tincq\t(%%rdx, %%rdi, 8)\n" )
      | _ -> () )
    | M_POST_DEC -> (
      match e with
      | loc, VAR var -> (
        Printf.fprintf out "\tdecq\t"; translate_address out (assoc_env var env); Printf.fprintf out "\n" )
      | loc, OP2 (S_INDEX, e1, e2) -> (
        Printf.fprintf out "\tdecq\t(%%rdx, %%rdi, 8)\n" )
      | _ -> () )
    | M_PRE_INC -> (
      Printf.fprintf out "\tincq\t%%rax\n";
      match e with
      | loc, VAR var -> (
        Printf.fprintf out "\tincq\t"; translate_address out (assoc_env var env); Printf.fprintf out "\n" )
      | loc, OP2 (S_INDEX, e1, e2) -> (
        Printf.fprintf out "\tincq\t(%%rdx, %%rdi, 8)\n" )
      | _ -> () )
    | M_PRE_DEC -> (
      Printf.fprintf out "\tdecq\t%%rax\n";
      match e with
      | loc, VAR var -> (
        Printf.fprintf out "\tdecq\t"; translate_address out (assoc_env var env); Printf.fprintf out "\n" )
      | loc, OP2 (S_INDEX, e1, e2) -> (
        Printf.fprintf out "\tdecq\t(%%rdx, %%rdi, 8)\n" )
      | _ -> () ))
  | loc, OP2(op, e1, e2) -> (
    translate_expr env e2;
    Printf.fprintf out "\tpushq\t%%rax\n";
    translate_expr env e1;
    Printf.fprintf out "\tpopq\t%%rdi\n";
    match op with
    | S_MUL -> Printf.fprintf out "\timulq\t%%rdi, %%rax\n"
    | S_ADD -> Printf.fprintf out "\taddq\t%%rdi, %%rax\n"
    | S_SUB -> Printf.fprintf out "\tsubq\t%%rdi, %%rax\n"
    | S_INDEX -> Printf.fprintf out "\tmovq\t%%rax, %%rdx\n\tmovq\t(%%rax, %%rdi, 8), %%rax\n"
    | S_DIV -> Printf.fprintf out "\tcqto\n\tidivq\t%%rdi\n"
    | S_MOD -> Printf.fprintf out "\tcqto\n\tidivq\t%%rdi\n\tmovq\t%%rdx, %%rax\n" )
  | loc, CMP(op, e1, e2) -> (
    let label = next_label_cmp () in
    translate_expr env e2;
    Printf.fprintf out "\tpushq\t%%rax\n";
    translate_expr env e1;
    Printf.fprintf out "\tpopq\t%%rdi\n";
    Printf.fprintf out "\tcmp\t%%rdi, %%rax\n";
    Printf.fprintf out "\tmovq\t$1, %%rax\n"; (
    match op with
    | C_LT -> Printf.fprintf out "\tjl\t%s\n" label
    | C_LE -> Printf.fprintf out "\tjle\t%s\n" label
    | C_EQ -> Printf.fprintf out "\tje\t%s\n" label );
    Printf.fprintf out "\txorq\t%%rax, %%rax\n";
    Printf.fprintf out "%s:\n" label )
  | loc, EIF(e1, e2, e3) -> (
    let label_false, label_end = next_label_eif () in
    translate_expr env e1;
    Printf.fprintf out "\tcmp\t$0, %%rax\n";
    Printf.fprintf out "\tje\t%s\n" label_false;
    translate_expr env e2;
    Printf.fprintf out "\tjmp\t%s\n" label_end;
    Printf.fprintf out "%s:\n" label_false;
    translate_expr env e3;
    Printf.fprintf out "%s:\n" label_end )
  | loc, ESEQ(e) ->
    List.iter (translate_expr env) e

and push_args out env expr_list =
  let rec push_args_aux i = function
    | e::t when i < 6 -> (
      Printf.fprintf out "\tpopq\t%s\n" (arg i); push_args_aux (succ i) t )
    | _ -> () in
  List.iter (fun e -> translate_expr out env e ; Printf.fprintf out "\tpushq\t%%rax\n") (List.rev expr_list);
  push_args_aux 0 expr_list

let rec env_code env = function
  | [] -> env
  | (CDECL (_, var))::t ->
    let a = next_address env in
    env_code ((var, a)::env) t

(* [translates_code out env code] returns the assembly for code [code] in environment [env] *)
let rec translate_code out env = let translate_code = translate_code out in function
  | loc, CBLOCK(decl, codes) -> (
    let env = env_code env decl in
    if List.length decl <> 0 then Printf.fprintf out "\tsubq\t$%d, %%rsp\n" (Pervasives.( * ) 8 (List.length decl));
    List.iter (translate_code env) codes;
    if List.length decl <> 0 then Printf.fprintf out "\taddq\t$%d, %%rsp\n" (Pervasives.( * ) 8 (List.length decl)) )
  | loc, CEXPR(e) -> translate_expr out env e
  | loc, CIF(e, c1, c2) -> (
    let label_false, label_end = next_label_if () in
    translate_expr out env e;
    Printf.fprintf out "\tcmp\t$0, %%rax\n";
    Printf.fprintf out "\tje\t%s\n" label_false;
    translate_code env c1;
    Printf.fprintf out "\tjmp\t%s\n" label_end;
    Printf.fprintf out "%s:\n" label_false;
    translate_code env c2;
    Printf.fprintf out "%s:\n" label_end )
  | loc, CWHILE(e, code) -> (
    let label_begin, label_end = next_label_while () in
    Printf.fprintf out "%s:\n" label_begin;
    translate_expr out env e;
    Printf.fprintf out "\tcmp\t$0, %%rax\n";
    Printf.fprintf out "\tje\t%s\n" label_end;
    translate_code env code;
    Printf.fprintf out "\tjmp\t%s\n" label_begin;
    Printf.fprintf out "%s:\n" label_end )
  | loc, CRETURN(some_e) -> ((
    match some_e with
      | Some e -> translate_expr out env e;
      | None -> () );
    Printf.fprintf out "\tjmp\t.%s_end\n" (!current_function) )

(* [translates_decl env decl] returns the new environent and the assembly for decl [decl] in environment [env] *)
and translate_decl out env = function
  | CDECL (_, var) -> (
    Printf.fprintf out ".data\n";
    Printf.fprintf out "\t.comm %s, 8, 8\n" var;
    Printf.fprintf out "\t.align 8\n";
    Printf.fprintf out ".text\n" )
  | CFUN (loc, func, args, code) -> (
    current_function := func;
    Printf.fprintf out "%s:\n" func;
    Printf.fprintf out "\tenter\t$0, $0\n";
    let len = List.length args in
    let rec push_arg_registers = function
      | i when i = len || i = 6 -> ()
      | i -> (
        Printf.fprintf out "\tpushq\t%s\n" (arg i);
        push_arg_registers (succ i) ) in
    let rec first n = function
      | h::t when n > 0 ->
        let f, l = first (pred n) t in
        h::f, l
      | _::_ as l -> [], l
      | [] -> [], [] in
    let high_addresses env =
      let rec aux env i = function
      | (CDECL (_, var))::t -> aux ((var, Offset (8L * Int64.of_int i))::env) (succ i) t
      | [] -> env in
      aux env 2 in
    push_arg_registers 0;
    let f, l = first 6 args in
    let env = env_code (high_addresses env l) f in
    translate_code out env code;
    Printf.fprintf out ".%s_end:\n" func;
    Printf.fprintf out "\tleave\n";
    if len > 6 then Printf.fprintf out "\tret $%d\n" (Pervasives.(-) len 6) else Printf.fprintf out "\tret\n" )

let compile out decl_list =
  Printf.fprintf out ".text\n";
  Printf.fprintf out "\t.globl main\n";
  List.iter (translate_decl out []) decl_list
