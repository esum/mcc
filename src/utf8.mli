(** type of an utf8 string *)
type utf8string


(** [utf8char_of_int u] returns the string containing the bytes for the Unicode character [u] *)
val utf8char_of_int : int -> string

(** [make n chr] returns a new utf8string with size [n] and characters equals to [chr] *)
val make : int -> string -> utf8string

(** [length str] returns the length of [str] *)
val length : utf8string -> int

(** [utf8 str] returns the converted utf8string from [str] *)
val utf8 : string -> utf8string

(** [get str i] returns the [i]th utf8 character of the utf8string [str] *)
val get : utf8string -> int -> string

(** [set str i chr] sets the [i]th character of the utf8string [str] to [chr] *)
val set : utf8string -> int -> string -> unit

(** [char_eq str i chr] returns true if and only if [get str i] equals [chr] *)
val char_eq : utf8string -> int -> string -> bool

(** [string_of_utf8 str] returns the utf8string [str] converted as a string *)
val string_of_utf8string : utf8string -> string

(** [print str] prints the utf8string [str] *)
val print : utf8string -> unit
