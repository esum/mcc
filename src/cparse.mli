type mon_op = M_MINUS | M_NOT | M_POST_INC | M_POST_DEC | M_PRE_INC | M_PRE_DEC
(** The unary operations:
  M_MINUS: computes the opposite -e of e;
  M_NOT: computes the logical negation ~e of e;
  M_POST_INC: post-incrementation e++;
  M_POST_DEC: post-decrementation e--;
  M_PRE_INC: pre-incrementation ++e;
  M_PRE_DEC: pre-decrementation --e.
  *)

type bin_op = S_MUL | S_DIV | S_MOD | S_ADD | S_SUB | S_INDEX
(** The binary operations:
  S_MUL: integer multiplication;
  S_DIV: integer division (quotient);
  S_MOD: integer division (remainder);
  S_ADD: integer addition;
  S_SUB: integer substraction;
  S_INDEX: access an array element a[i].
  *)

type cmp_op = C_LT | C_LE | C_EQ
(** The comparison operations:
  C_LT (less than): <;
  C_LE (less than or equal to): <=;
  C_EQ (equal): ==.
  *)

type loc_expr = Error.locator * expr
and expr = 
    VAR of string (** a variable --- always of type int. *)
  | CST of int64 (** an integer constant. *)
  | STRING of string (** a string constant. *)
  | SET_VAR of string * loc_expr (** assigment x=e. *)
  | SET_ARRAY of string * loc_expr * loc_expr (** assigment x[e]=e'. *)
  | CALL of string * loc_expr list (** function call f(e1,...,en) *)

  | OP1 of mon_op * loc_expr
    (** OP1(mop, e) denotes -e, ~e, e++, e--, ++e, or --e. *)
  | OP2 of bin_op * loc_expr * loc_expr
    (** OP2(bop,e,e') denotes e*e', e/e', e%e',
                              e+e', e-e', or e[e']. *)
  | CMP of cmp_op * loc_expr * loc_expr
    (** CMP(cop,e,e') denotes e<e', e<=e', or e==e' *)
  | EIF of loc_expr * loc_expr * loc_expr
    (** EIF(e1,e2,e3) denotes e1?e2:e3 *)
  | ESEQ of loc_expr list
    (** e1, ..., en [sequence, equivalent to e1;e2];
      represents skip if n=0. *)

type var_declaration =
  | CDECL of Error.locator * string
    (** variable declaration of type int. *)
  | CFUN of Error.locator * string * var_declaration list * loc_code
    (** function with its arguments, and its code. *)
and loc_code = Error.locator * code
and code =
    CBLOCK of var_declaration list * loc_code list (** { declarations; code; } *)
  | CEXPR of loc_expr (** an expression e; seen as an instruction. *)
  | CIF of loc_expr * loc_code * loc_code (** if (e) c1; else c2; *)
  | CWHILE of loc_expr * loc_code (** while (e) c1; *)
  | CRETURN of loc_expr option (** return; ou return (e); *)

val cline : int ref
val ccol : int ref
val oldcline : int ref
val oldccol : int ref
val cfile : string ref

val getloc : unit -> string * int * int * int * int

val loc_of_expr : Error.locator*'a -> Error.locator
val e_of_expr : loc_expr -> expr
