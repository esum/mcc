type utf8char = bytes

type utf8string = utf8char array


let utf8_length str =
  let len = String.length str in
  let pos = ref 0 in
  let k = ref 0 in
  while (!pos < len) do
    let c = int_of_char str.[!pos] in
    if c >= 240 then begin
      pos := !pos + 4;
      incr k; end
    else if c >= 224  then begin
      pos := !pos + 3;
      incr k; end
    else if c >= 192  then begin
      pos := !pos + 2;
      incr k; end
    else begin
      pos := !pos + 1;
      incr k; end
  done;
  !k

let utf8char_of_int u =
  if u < 128 then
    String.make 1 (char_of_int u)
  else if u < 2048 then begin
    let res = Bytes.create 2 in
    Bytes.set res 0 (char_of_int (192 lor (u lsr 6)));
    Bytes.set res 1 (char_of_int (128 lor (u land 63)));
    Bytes.to_string res end
  else if u < 65536 then begin
    let res = Bytes.create 3 in
    Bytes.set res 0 (char_of_int (224 lor (u lsr 12)));
    Bytes.set res 1 (char_of_int (128 lor ((u lsr 6) land 63)));
    Bytes.set res 2 (char_of_int (128 lor (u land 63)));
    Bytes.to_string res end
  else if u < 1114111 then begin
    let res = Bytes.create 4 in
    Bytes.set res 0 (char_of_int (240 lor (u lsr 18)));
    Bytes.set res 1 (char_of_int (128 lor ((u lsr 12) land 63)));
    Bytes.set res 2 (char_of_int (128 lor ((u lsr 6) land 63)));
    Bytes.set res 3 (char_of_int (128 lor (u land 64)));
    Bytes.to_string res end
  else
    ""

let make_char = Bytes.of_string

let make n chr =
  let res = Array.make n (Bytes.create 1) in
  for i=0 to (n-1) do
      res.(i) <- make_char chr
  done;
  res

let length = Array.length

let utf8 str =
  let len = String.length str in
  let utf8len = utf8_length str in
  let res = make utf8len " " in
  let pos = ref 0 in
  let k = ref 0 in
  while !pos < len do
    let c = int_of_char str.[!pos] in
    let l = match c with
      | c when c >= 240 -> 4 
      | c when c >= 224 -> 3
      | c when c >= 192 -> 2
      | _ -> 1 in
    res.(!k) <- Bytes.create l; 
    for i=0 to (l-1) do
      Bytes.set res.(!k) i str.[!pos+i]
    done;
    pos := !pos + l;
    incr k
  done;
  res

let get str i =
  Bytes.to_string str.(i)

let set str i chr =
  str.(i) <- make_char chr

let char_eq str i chr =
  str.(i) = make_char chr

let string_of_utf8string str =
  let size = ref 0 in
  let len = length str in
  for i=0 to (len-1) do
    size := !size + (Bytes.length (str.(i)))
  done;
  let res = Bytes.create (!size) in
  let i = ref 0 in
  let i' = ref 0 in
  while !i' < !size do
    let j = ref 0 in
    while !j < (Bytes.length (str.(!i))) do
       Bytes.set res (!i+(!j)) (Bytes.get str.(!i') !j);
       incr j
    done;
    i := !i + (!j);
    incr i'
  done;
  Bytes.to_string res

let print str =
  let len = length str in
  for i=0 to (len-1) do
    print_string (Bytes.to_string (str.(i)))
  done;
