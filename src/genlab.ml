
(*
 *  Copyright (c) 2005 by Laboratoire Spécification et Vérification (LSV),
 *  CNRS UMR 8643 & ENS Cachan.
 *  Written by Jean Goubault-Larrecq.  Not derived from licensed software.
 *
 *  Permission is granted to anyone to use this software for any
 *  purpose on any computer system, and to redistribute it freely,
 *  subject to the following restrictions:
 *
 *  1. Neither the author nor its employer is responsible for the consequences of use of
 *    this software, no matter how awful, even if they arise
 *    from defects in it.
 *
 *  2. The origin of this software must not be misrepresented, either
 *    by explicit claim or by omission.
 *
 *  3. Altered versions must be plainly marked as such, and must not
 *    be misrepresented as being the original software.
 *
 *  4. This software is restricted to non-commercial use only.  Commercial
 *    use is subject to a specific license, obtainable from LSV.
 *)

open Printf

let counter = ref 0

let counter_cmp = ref (-1)
let counter_while = ref (-1)
let counter_if = ref (-1)
let counter_eif = ref (-1)

let rec genlab func =
  incr counter;
  sprintf ".%s_%d" func (!counter)

let next_label_cmp () =
  incr counter_cmp;
  sprintf ".cmp%d_false" (!counter_cmp)

let next_label_while () =
  incr counter_while;
  sprintf ".while%d" (!counter_while), sprintf ".while%d_end" (!counter_while)

let next_label_if () =
  incr counter_if;
  sprintf ".if%d_false" (!counter_if), sprintf ".if%d_end" (!counter_if)

let next_label_eif () =
  incr counter_eif;
  sprintf ".eif%d_false" (!counter_eif), sprintf ".eif%d_end" (!counter_eif)


let labelize loc =
  let (name, fl, fc, ll, lc) = loc in
  let len = String.length name in
  let filename_begin = ref 0 in
  let filename_end = ref len in
  for i=(len-1) downto 0 do
    if name.[i] = '.' && !filename_end = len then
      filename_end := i;
    if name.[i] = '/' && !filename_begin = 0 then
      filename_begin := i+1;
  done;
  sprintf ".%s_%d_%d_%d_%d" (String.sub name (!filename_begin) (!filename_end-(!filename_begin))) fl fc ll lc
